from flask import Flask, render_template, redirect, url_for,session, jsonify, request, send_from_directory, abort
import os,json
import time
from crawl import Crawl
from flask import jsonify
from flask_restful import Resource, Api

app = Flask(__name__,
            static_url_path='/assets', 
                static_folder='assets',
                template_folder='templates')
app.secret_key = os.getcwd() +'secret_key'
app.debug = True
api = Api(app)

@app.route('/', methods=['GET'])
def index():
    return render_template('index.html')

class GPATMU(Resource):
    # def get(self):
    #     return render_template('index.html')
    def post(self):
        try:
            msv = request.json['msv']
            password = request.json['password']

            data = {
                "Mạng máy tính và truyền thông": {
                    "tc": 2,
                    "diem": 4
                },
                "Toán cao cấp 1": {
                    "tc": 2,
                    "diem": 2.5
                },
                "Cơ sở dữ liệu": {
                    "tc": 2,
                    "diem": 2
                },
                "Thương mại điện tử căn bản": {
                    "tc": 3,
                    "diem": 4
                },
                "Giáo dục thể chất chung *": {
                    "tc": 1,
                    "diem": 3.5
                },
                "Cơ sở lập trình": {
                    "tc": 3,
                    "diem": 4
                },
                "Kinh tế học": {
                    "tc": 3,
                    "diem": 3
                },
                "Triết học Mác - Lênin": {
                    "tc": 3,
                    "diem": 3
                },
                "Phương pháp nghiên cứu khoa học": {
                    "tc": 2,
                    "diem": 3.5
                },
                "Pháp luật đại cương": {
                    "tc": 2,
                    "diem": 4
                },
                "Thực tập nhận thức nghề nghiệp": {
                    "tc": 2,
                    "diem": 4
                },
                "Đồ họa căn bản": {
                    "tc": 2,
                    "diem": 4
                },
                "Lý thuyết xác suất và thống kê toán": {
                    "tc": 3,
                    "diem": 3
                },
                "Quản trị học": {
                    "tc": 3,
                    "diem": 2
                },
                "Tiếng Anh chuyên ngành 1": {
                    "tc": 2,
                    "diem": 2
                },
                "Cầu lông *": {
                    "tc": 1,
                    "diem": 4
                },
                "Phân tích và thiết kế hệ thống thông tin": {
                    "tc": 3,
                    "diem": 3.5
                },
                "Lập trình hướng đối tượng": {
                    "tc": 3,
                    "diem": 4
                },
                "Hệ thống thông tin quản lý": {
                    "tc": 3,
                    "diem": 3
                },
                "Toán cao cấp 2": {
                    "tc": 2,
                    "diem": 1
                },
                "Cờ vua *": {
                    "tc": 1,
                    "diem": 2
                },
                "Chủ nghĩa xã hội khoa học": {
                    "tc": 2,
                    "diem": 2.5
                },
                "Kinh tế chính trị Mác - Lênin": {
                    "tc": 2,
                    "diem": 3
                },
                "Thiết kế và triển khai website": {
                    "tc": 3,
                    "diem": 4
                },
                "Thực hành khai thác dữ liệu trên mạng Internet": {
                    "tc": 2,
                    "diem": 4
                },
                "Tiếng Anh chuyên ngành 2": {
                    "tc": 2,
                    "diem": 3
                },
                "Tư tưởng Hồ Chí Minh": {
                    "tc": 2,
                    "diem": 3
                },
                "Khai phá dữ liệu trong kinh doanh": {
                    "tc": 2,
                    "diem": 3.5
                },
                "Các phần mềm ứng dụng trong doanh nghiệp": {
                    "tc": 3,
                    "diem": 4
                },
                "Quản trị cơ sở dữ liệu": {
                    "tc": 3,
                    "diem": 4
                },
                "Nhập môn tài chính - tiền tệ": {
                    "tc": 3,
                    "diem": 3.5
                },
                "Nguyên lý kế toán": {
                    "tc": 3,
                    "diem": 4
                },
                "Lịch sử Đảng Cộng sản Việt Nam": {
                    "tc": 2,
                    "diem": 3
                },
                "Kiểm thử phần mềm": {
                    "tc": 3,
                    "diem": 4
                },
                "Thực hành thiết kế và triển khai website với PHP": {
                    "tc": 3,
                    "diem": 4
                },
                "Thực hành quản trị hệ thống thông tin doanh nghiệp": {
                    "tc": 3,
                    "diem": 4
                },
                "Thực hành phân tích và thiết kế hệ thống thông tin": {
                    "tc": 3,
                    "diem": 4
                },
                "Đồ họa ứng dụng": {
                    "tc": 3,
                    "diem": 4
                },
                "Thực hành lập trình hướng đối tượng (Java)": {
                    "tc": 3,
                    "diem": 4
                },
                "Thực hành quản trị cơ sở dữ liệu (định hướng nghề nghiệp)": {
                    "tc": 3,
                    "diem": 4
                },
                "An toàn và bảo mật thông tin": {
                    "tc": 3,
                    "diem": 3
                },
                "Quản trị hệ thống thông tin doanh nghiệp": {
                    "tc": 3,
                    "diem": 2
                },
                "Phát triển hệ thống thông tin kinh tế": {
                    "tc": 3,
                    "diem": 3.5
                },
                "Báo cáo thực tập": {
                    "tc": 3,
                    "diem": 4
                }
            }
            crawl = Crawl()
            data = crawl.handle(msv, password)
            return { 'data': data }
        except IndexError:
            abort(404)
api.add_resource(GPATMU, '/api/login')

# @app.route('/', methods=['GET', 'POST'])
# def index():
#     if request.method == 'POST':
#         return render_template('index.html')
        
#         msv = request.form['msv']
#         password = request.form['password']
#         response = jsonify({ 'hihi': 'haha' })
#         response.headers.add('Access-Control-Allow-Origin', '*')
        
#         return response
#         return '1'
#         return {
#             msv,
#             password
#         }
        
#     return render_template('index.html')


if __name__ == "__main__":
    app.run()