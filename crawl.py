# from bs4 import BeautifulSoup 
# import io
import time
from selenium.webdriver import Chrome 
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from webdriver_manager.chrome import ChromeDriverManager
# import chromedriver_binary

URL = "https://congdaotao.tmu.edu.vn/login" 
URL_SCORE = "https://congdaotao.tmu.edu.vn/student/marks"

class Crawl():
    def __init__(self):
        self.chrome_options = Options()
        # self.chrome_options.add_argument("--incognito")
        self.chrome_options.add_argument('--headless')
        # self.chrome_options.add_argument('--no-sandbox')
        self.chrome_options.add_argument('--disable-dev-shm-usage')
        # self.chrome_options.add_argument("--disable-gpu")
        self.chrome_options.binary_location = '/opt/render/project/src/chrome'
        self.driver = Chrome(chrome_options = self.chrome_options, executable_path='chromedriver')
        # self.driver = Chrome(ChromeDriverManager().install(), chrome_options = self.chrome_options)
    
    def handle(self, msv, password):
        self.driver.get(URL)
        username = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@id="root"]/div[2]/div[2]/div/div/div[3]/form/div[1]/div/input')))
        username.send_keys('19D191063')

        password = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@id="root"]/div[2]/div[2]/div/div/div[3]/form/div[2]/div/input')))
        password.send_keys('Thuyanh1x2@')

        button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[2]/div[2]/div/div/div[3]/form/div[3]/button')))
        button.click()
        
        print('login success')

        time.sleep(3)
        
        self.driver.get(URL_SCORE)
        print('go to score page')
        
        time.sleep(3)
        gpa = self.driver.execute_script(open("execute_script.js").read())
        self.driver.close()
        return gpa
    
crawl = Crawl()
data = crawl.handle('19D191063', 'Thuyanh1x2@')
print (data)