#!/usr/bin/env bash
# exit on error
# set -o errexit

# STORAGE_DIR= $PWD

if [[ ! -d $STORAGE_DIR/chrome ]]; then
  echo "...Downloading Chrome"
  mkdir -p chrome
  cd chrome
  wget -P ./ https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
  dpkg -x ./google-chrome-stable_current_amd64.deb /opt/render/project/src/chrome
  rm ./google-chrome-stable_current_amd64.deb
  cd /opt/render/project/src # Make sure we return to where we were
  echo "...Done Install Chrome"
else
  echo "...Using Chrome from cache"
fi

# be sure to add Chromes location to the PATH as part of your Start Command
# export PATH="${PATH}:/opt/render/project/.render/chrome/opt/google/chrome"

# add your own build commands...
